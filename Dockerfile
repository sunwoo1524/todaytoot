FROM node:21

WORKDIR /app

COPY . .

WORKDIR /app/frontend

RUN npm install

RUN npm run build

WORKDIR /app

RUN npm install

ENTRYPOINT ["sh", "/app/docker-entrypoint.sh"]
