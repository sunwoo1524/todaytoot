interface response {
    ok: boolean
    status: number
    res: Object | null
}

export const sendAPI = async (dir: string, method: string, body: Object | undefined): Promise<response> => {
    let res;
    if (body === undefined) {
        res = await fetch(dir, {
            method
        })   
    } else {
        res = await fetch(dir, {
            method,
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        })
    }
    
    if (!res.ok) {
        let refresh_res = await fetch("/api/users/refresh_token", {
            method: "PUT"
        })

        if (!refresh_res.ok) {
            return {
                ok: false,
                status: res.status,
                res: null
            }
        }

        if (body === undefined) {
            res = await fetch(dir, {
                method
            })   
        } else {
            res = await fetch(dir, {
                method,
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body)
            })
        }
    }

    const data = await res.json()

    return {
        ok: true,
        status: res.status,
        res: data
    }
}