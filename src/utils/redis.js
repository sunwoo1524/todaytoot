import { createClient } from 'redis'
import dotenv from "dotenv";

dotenv.config()

const client = createClient({
    url: process.env.REDIS_URL,
})

async function start() {
    await client.connect()
}

export const getRefreshTokens = async (id) => {
    const tokens = await client.lRange(id, 0, -1)
    return tokens
}

export const pushRefreshToken = async (id, token) => {
    await client.lPush(id, token)
}

export const deleteRefreshToken = async (id, token) => {
    const tokens = await getRefreshTokens(id)
    const index = tokens.indexOf(token)

    if (index === -1) {
        client.lPop(id, index + 1)
    }
}

start()
