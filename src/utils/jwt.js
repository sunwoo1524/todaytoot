import jwt from "jsonwebtoken";
import dotenv from "dotenv";

import { getRefreshTokens } from "./redis.js";

dotenv.config()

export const generateTodaytootToken = (id) =>{
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: "30m"
    })
}

export const generateRefreshToken = () => {
    const token = jwt.sign({}, process.env.JWT_SECRET, {
        expiresIn: "30 days"
    })

    return token
}

export const verifyTodaytootToken = (token) => {
    let decoded
    try {
        decoded = jwt.verify(token, process.env.JWT_SECRET)
    } catch {
        return {success: false}
    }
    
    return {success: true, payload: decoded}
}

export const verifyRefreshToken = async (id, token) => {
    try {
        jwt.verify(token, process.env.JWT_SECRET)
    } catch {
        return false
    }

    // find refresh token in redis
    const tokens = await getRefreshTokens(id)

    if (!tokens.includes(token)) {
        return false
    }

    return true
}

export const getIDFromToken = (token) => {
    return jwt.decode(token).id
}
