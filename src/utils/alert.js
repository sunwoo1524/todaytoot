import {prisma} from "../lib/prisma.js"

function calcDelta(current, previous) {
    let result = current - previous
    if (result > 0) {
        return "+" + String(result)
    }
    return String(result)
}

const postStatsToot = async ({ id, access_token, instance, followers_count, following_count, statuses_count, status_template, status_visibility }) => {
    try {
        // get current user's data
        let mst_res = await fetch(`https://${instance}/api/v1/accounts/verify_credentials`, {
            headers: {
                "Authorization": "Bearer " + access_token
            }
        })
        if (!mst_res.ok) return
        mst_res = await mst_res.json()

        // replace variables
        /**
         * @type {string}
         */
        let status = status_template
        status = status.replace(/{statuses_count}/g, mst_res.statuses_count)
        status = status.replace(/{statuses_delta}/g, calcDelta(mst_res.statuses_count, statuses_count))
        status = status.replace(/{following_count}/g, mst_res.following_count)
        status = status.replace(/{following_delta}/g, calcDelta(mst_res.following_count, following_count))
        status = status.replace(/{followers_count}/g, mst_res.followers_count)
        status = status.replace(/{followers_delta}/g, calcDelta(mst_res.followers_count, followers_count))

        // post
        let status_res = await fetch(`https://${instance}/api/v1/statuses`, {
            method: "POST",
            headers: {
                "Authorization": "Bearer " + access_token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                status,
                visibility: status_visibility
            })
        })
        
        if (status_res.ok) {
            status_res = await status_res.json()
            console.log("Posted stats toot successfully: " + instance, " | ", status_res.id)
        } else {
            console.log("Failed to post stats toot: " + instance, " | ", status_res.status)
        }

        // update data
        await prisma.user.update({
            where: {
                id
            },
            data: {
                statuses_count: mst_res.statuses_count,
                following_count: mst_res.following_count,
                followers_count: mst_res.followers_count
            }
        })
    } catch (err) {
        console.log(err)
    }
}

export const alertToAllUsers = async () => {
    const all_users = await prisma.user.findMany()
    all_users.forEach(async user => {
        await postStatsToot(user)
    })
}