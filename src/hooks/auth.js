import { verifyTodaytootToken } from "../utils/jwt.js"

export default async function authenticate(req, res) {
    const todaytoot_token = req.cookies.todaytoot_token

    if (todaytoot_token === null || todaytoot_token === undefined) {
        const err = new Error()
        err.statusCode = 401
        throw err
    }

    const { success, payload } = verifyTodaytootToken(todaytoot_token)

    if (!success) {
        const err = new Error()
        err.statusCode = 401
        throw err
    }

    req.decoded = payload
}
