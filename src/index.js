import Fastify from "fastify"
import cookie from '@fastify/cookie'
import fastifyStatic from "@fastify/static"
import dotenv from "dotenv"
import path from "path"
import * as url from 'url';
import cron from "node-cron"

import authRoutes from "./routes/auth.js"
import { alertToAllUsers } from "./utils/alert.js"

const __dirname = url.fileURLToPath(new URL('.', import.meta.url))

dotenv.config()

const fastify = Fastify({
  logger: true
})

fastify.register(cookie, {
    secret: process.env.COOKIE_SECRET,
    setOptions: {}
})

fastify.register(authRoutes, { prefix: "/api/users" })

// FE
fastify.register(fastifyStatic, {
    root: path.join(__dirname, "../frontend/dist")
})
fastify.get("/", (req, res) => {
    return res.sendFile("index.html")
})

const start = async () => {
    try {
        await fastify.listen({ port: 3030, host: "0.0.0.0" })

        cron.schedule("0 0 * * *", async () => await alertToAllUsers())
    } catch (err) {
        fastify.log.error(err)
        process.exit(1)
    }
}

start()
