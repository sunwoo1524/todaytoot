import fastifyPlugin from "fastify-plugin"

import { prisma } from "../lib/prisma.js"
import { generateTodaytootToken, generateRefreshToken, verifyRefreshToken, getIDFromToken } from "../utils/jwt.js"
import { deleteRefreshToken, pushRefreshToken } from "../utils/redis.js"
import authenticate from "../hooks/auth.js"

/**
 * @param {import('fastify').FastifyInstance} fastify
 */
export default async function routes(fastify) {
    // sign in
    fastify.post("/", {
        schema: {
            body: {
                type: "object",
                properties: {
                    access_token: {type: "string"},
                    instance: {type: "string"}
                }
            }
        }
    }, async (req, res) => {
        let { access_token, instance } = req.body

        instance = instance.toLowerCase()

        // get user's data
        let user_res;

        try {
            user_res = await fetch(`https://${instance}/api/v1/accounts/verify_credentials`, {
                method: "GET",
                headers: {
                    "Authorization": `Bearer ${access_token}`
                }
            })
        } catch {
            // fucking server error
            const err = new Error()
            err.statusCode = 400
            throw err
        }
        const { followers_count, following_count, statuses_count, username } = await user_res.json()

        // check if user is valid
        if (!user_res.ok) {
            const err = new Error()
            err.statusCode = 400
            throw err
        }

        // get user from db
        let db_user = await prisma.user.findFirst({
            where: {
                instance,
                username,
            }
        })

        // register if user is exist
        if (db_user === null || db_user === undefined) {
            // store to db
            db_user = await prisma.user.create({
                data: {
                    access_token,
                    instance,
                    username,
                    followers_count,
                    following_count,
                    statuses_count
                }
            })
        } else {
            // update new access token if not
            db_user = await prisma.user.update({
                where: {
                    id: db_user.id
                },
                data: {
                    access_token
                }
            })
        }

        // generate todaytoot access token and refresh token
        const todaytoot_token = generateTodaytootToken(db_user.id)
        const refresh_token = generateRefreshToken()

        res.setCookie("todaytoot_token", todaytoot_token, {
            httpOnly: true,
            path: "/",
            expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 30) // 30days
        })
        res.setCookie("refresh_token", refresh_token, {
            httpOnly: true,
            path: "/",
            expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 30) // 30 days
        })

        pushRefreshToken(db_user.id, refresh_token)

        return {"message": "Signed in successfully"}
    })

    // refresh access token
    fastify.put("/refresh_token", {
    }, async (req, res) => {
        const todaytoot_token = req.cookies.todaytoot_token
        const refresh_token = req.cookies.refresh_token

        if (todaytoot_token === undefined || refresh_token === undefined) {
            const err = new Error("TOKENS_NOT_FOUND")
            err.statusCode = 401
            throw err
        }

        // decode todaytoot access token
        const id = getIDFromToken(todaytoot_token)
        
        // check if refresh token is valid
        if (! await verifyRefreshToken(id, refresh_token)) {
            const err = new Error("INVALID_REFRESH_TOKEN")
            err.statusCode = 401
            throw err
        }

        // generate new todaytoot access token
        const new_todaytoot_token = generateTodaytootToken(id)
        res.setCookie("todaytoot_token", new_todaytoot_token, {
            path: "/",
            expires: new Date(Date.now() +1000 * 60 * 60 * 24 * 30) // 30 days
        })

        return {"message": "Refreshed todaytoot token successfully"}
    })

    // log out
    fastify.get("/log_out", {
        onRequest:[authenticate]
    }, async(req, res)=>{
        const { id } = req.decoded
        deleteRefreshToken(id, req.cookies.refresh_token)
        res.clearCookie("todaytoot_token")
        res.clearCookie("refresh_token")
        return {"message":"Cleared cookies successfully"}
    })

    // delete account from todaytoot
    fastify.delete("/", {
        onRequest:[authenticate]
    }, async(req, res)=>{
        const { id } = req.decoded
        const delete_user = await prisma.user.delete({
            where: {
                id,
            },
        })
        deleteRefreshToken(id, req.cookies.refresh_token)
        res.clearCookie("todaytoot_token")
        res.clearCookie("refresh_token")
        return {"message":"Deleted the account from todaytoot"}
    })

    // get user
    fastify.get("/user", {
        onRequest: [authenticate]
    }, async (req, res) => {
        const { id } = req.decoded
        const db_user = await prisma.user.findFirst({
            where: {
                id
            }
        })

        return db_user
    })

    // update setting
    fastify.put("/user/setting", {
        onRequest:[authenticate],
        schema: {
            body: {
                type: "object",
                properties: {
                    status_template: {type:"string"},
                    status_visibility: {type: "string"}
                }
            }
        }
    }, async (req, res) => {
        const { id } = req.decoded
        const { status_template, status_visibility } = req.body

        const db_user = await prisma.user.update({
            where: {
                id
            },
            data: {
                status_template,
                status_visibility
            }
        })

        return {"message": "Updated setting successfully"}
    })
}
