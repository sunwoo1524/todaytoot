dockerize -wait tcp://postgres:5432 -timeout 20s
echo "Start server"
npx prisma db push
npx prisma generate dev --name init
npm run start
