# Todaytoot
> Toot your mastodon statistics every AM 12:00

## Install
Copy `.env` and `docker-compose.yml`.
```sh
cp .env.example .env
cp docker-compose.example.yml docker-compose.yml
```
Edit `.env` using your favorite text editor.
```sh
vim .env
# or
nano .env
```
You just should edit these.
- DATABASE_URL: change `db_password` to your password
- POSTGRES_PASSWORD: change to equal password of DATABASE_URL
- COOKIE_SECRET, JWT_SECRET: change to loooong random string

Build & run as daemon
```sh
docker compose up -d
```

### Dev install
```sh
cd frontend
npm i
cd ..
npm i
npx prisma db push
npx prisma migrate dev --name init
npm run dev # nodemon
```